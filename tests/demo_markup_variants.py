import random

LIST = [1, 2, 3, 4]
DICT = {
    "One": 1,
    "Two": 2,
    "Three": 3
}

def shuffled() -> list:
    shuffled_list = LIST.copy()
    random.shuffle(shuffled_list)
    return shuffled_list

def sort_list(input_list: list) -> list:
    return sorted(input_list)

def list_creator(*args) -> list:
    return args

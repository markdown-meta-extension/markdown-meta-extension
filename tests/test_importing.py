import unittest

from markdown_meta_extension.importing import Importer

class TestImporting(unittest.TestCase):

    def test_import_function(self):
        pass

    def test_import_datetime_parts_no_error(self):
        return Importer(
            {
                "now": "datetime:datetime.now",
                "Datetime": "datetime:datetime",
                "Date": "datetime:date"
            }
        )

    def test_import_file_module_parts_no_error(self):
        return Importer(
            {
                "demo_function": "./tests/demo_module.py:demo_function",
                "DemoClass": "./tests/demo_module.py:DemoClass",
                "demo_class_method": "./tests/demo_module.py:DemoClass.demo_class_method",
                "DEMO_OBJECT": "./tests/demo_module.py:DEMO_OBJECT",
                "demo_method": "./tests/demo_module.py:DEMO_OBJECT:demo_method"
            }
        )


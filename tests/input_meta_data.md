---
meta-data-import:
- ./tests/base_meta_data.yaml
meta-data:
  title: Inputting of Meta Data
import:
- ./tests/base_imports.yaml
---

# Inputting of Meta Data

{{ my_object = class
[]
}}

{{ my_object.demo_method
[]
}}

---
LIST: ./tests/demo_markup_variants.py:LIST
DICT: ./tests/demo_markup_variants.py:DICT
---

{{
another_list:
- 10
- 20
- 30
}}

{{ another_list
}}

## Inline List

This is inline text with an inline list like {{ LIST }}.

## Block List

{{ LIST
[]
}}

## Block Dict

{{ DICT
[]
}}

The first entry is {{ LIST.0 }} or {{ DICT.One [] }}.

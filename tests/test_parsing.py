from pathlib import Path
from typing import Text
import unittest

from bs4 import BeautifulSoup
import markdown
import markdown_meta_extension
import parse

class TestParsing(unittest.TestCase):

    def setUp(self):
        self.parser = markdown.Markdown(
            extensions=[
                markdown_meta_extension.MetaExtension()
            ]
        )

    def parse_file(self, file_path: Text) -> Text:
        file_path = Path(file_path)
        result = self.parser.convert(file_path.read_text())
        return result

    def test_parsing_now_no_error(self):
        self.parse_file("./tests/input_now.md")

    def test_parsing_now_correct_output(self):
        result = self.parse_file("./tests/input_now.md")
        datetime_pattern = "{:4d}-{:2d}-{:2d} {:2d}:{:2d}:{:2d}.{:6d}"
        self.assertTrue(
            parse.search(datetime_pattern, result)
        )

    def test_parsing_date_no_error(self):
        self.parse_file("./tests/input_date.md")

    def test_parsing_date_correct_output(self):
        result = self.parse_file("./tests/input_date.md")
        result = BeautifulSoup(result, features="lxml")
        self.assertTrue(
            result.find("div", text="2020-02-25")
        )

    def test_parsing_date_inline_no_error(self):
        self.parse_file("./tests/input_date_inline.md")

    def test_parsing_date_inline_correct_output(self):
        result = self.parse_file("./tests/input_date_inline.md")
        result = BeautifulSoup(result, features="lxml")
        self.assertTrue(
            result.find("span", text="2020-02-27")
        )

    def test_parsing_imports_no_error(self):
        self.parse_file("./tests/input_imports.md")

    def test_parsing_meta_data_no_error(self):
        self.parse_file("./tests/input_meta_data.md")

    def test_parsing_markup_variants_no_error(self):
        self.parse_file("./tests/input_markup_variants.md")

    def test_parsing_nesting_no_error(self):
        self.parse_file("./tests/input_nesting.md")

    def test_parsing_nesting_correct_output(self):
        result = self.parse_file("./tests/input_nesting.md")
        result = BeautifulSoup(result, features="lxml")
        self.assertTrue(
            result.find("li", text="1")
        )

    def test_parsing_variables_no_error(self):
        self.parse_file("./tests/input_variables.md")

    def test_parsing_variables_correct_output(self):
        result = self.parse_file("./tests/input_variables.md")
        result = BeautifulSoup(result, features="lxml")
        self.assertTrue(
            result.find("span", text="Test!")
        )
import inspect
import datetime
import unittest

from markdown_meta_extension.plugins import CallableWrapper

class TestCallableHandling(unittest.TestCase):

    def test_import_package_class_method_no_error(self):
        return CallableWrapper.import_callable(
            "today",
            ["datetime", "date.today"]
        )

    def test_import_package_class_method_call_not_none(self):
        callable_wrapper = self.test_import_package_class_method_no_error()
        self.assertIsNotNone(callable_wrapper())

    def test_import_package_class_method_call_result_correct_type(self):
        callable_wrapper = self.test_import_package_class_method_no_error()
        self.assertIsInstance(callable_wrapper(), datetime.date)

    def test_import_package_class_method_extra_no_error(self):
        return CallableWrapper.import_callable(
            "today",
            ["datetime", "date", "today"]
        )

    def test_import_package_class_method_extra_call_not_none(self):
        callable_wrapper = self.test_import_package_class_method_extra_no_error()
        self.assertIsNotNone(callable_wrapper())

    def test_import_package_class_method_extra_call_result_correct_type(self):
        callable_wrapper = self.test_import_package_class_method_extra_no_error()
        self.assertIsInstance(callable_wrapper(), datetime.date)

    def test_import_package_class_no_error(self):
        return CallableWrapper.import_callable("today", ["datetime", "date"])

    def test_import_package_class_is_class(self):
        callable_wrapper = self.test_import_package_class_no_error()
        self.assertTrue(
            inspect.isclass(
                callable_wrapper.imported_callable
            )
        )

    def test_import_package_class_call_not_none(self):
        callable_wrapper = self.test_import_package_class_no_error()
        self.assertIsNotNone(
            callable_wrapper(
                {
                    "year": 2020,
                    "month": 1,
                    "day": 28
                }
            )
        )

    def test_import_package_class_call_result_correct_type(self):
        callable_wrapper = self.test_import_package_class_no_error()
        self.assertIsInstance(
            callable_wrapper(
                {
                    "year": 2020,
                    "month": 1,
                    "day": 28
                }
            ),
            datetime.date
        )

    def test_import_file_non_existent(self):
        callable_wrapper = CallableWrapper.import_callable(
            "non_existent_file_function",
            [
                "./tests/non_existent_file.py",
                "demo_function"
            ]
        )

        self.assertIsNone(
            callable_wrapper
        )


    def test_import_file_function_no_error(self):
        return CallableWrapper.import_callable(
            "demo_function",
            [
                "./tests/demo_module.py",
                "demo_function"
            ]
        )

    def test_import_file_class_no_error(self):
        return CallableWrapper.import_callable(
            "DemoClass",
            [
                "./tests/demo_module.py",
                "DemoClass"
            ]
        )

    def test_import_file_class_method_no_error(self):
        return CallableWrapper.import_callable(
            "demo_class_method",
            [
                "./tests/demo_module.py",
                "DemoClass",
                "demo_class_method"
            ]
        )

    def test_import_file_object_no_error(self):
        return CallableWrapper.import_callable(
            "DEMO_OBJECT",
            [
                "./tests/demo_module.py",
                "DEMO_OBJECT"
            ]
        )

    def test_import_file_object_method_no_error(self):
        return CallableWrapper.import_callable(
            "demo_object_method",
            [
                "./tests/demo_module.py",
                "DEMO_OBJECT",
                "demo_method"
            ]
        )

    def test_import_file_jinja_single_html_template_no_error(self):
        return CallableWrapper.import_callable(
            "demo_template",
            [
                "./tests/demo_template.html"
            ]
        )

    def test_import_file_jinja_single_html_template_call(self):
        callable_wrapper = self.test_import_file_jinja_single_html_template_no_error()

        self.assertEqual(
            str(callable_wrapper(arguments={})).strip(),
            "No Argument <em>demo</em> given."
        )
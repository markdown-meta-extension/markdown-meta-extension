from typing import Text


def demo_function() -> Text:
    return "Demo"

def demo_void_function():
    pass

def demo_echo_function(echo: Text):
    return echo

class DemoClass:

    def __init__(self):
        pass

    @classmethod
    def demo_class_method(cls) -> Text:
        return "Demo"

    def demo_method(self) -> Text:
        return "Demo"

DEMO_OBJECT = DemoClass()

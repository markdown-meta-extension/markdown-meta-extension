FROM python:3.8.2
WORKDIR /markdown-meta-extension
COPY setup.py .
COPY ./markdown_meta_extension ./markdown_meta_extension
RUN pip install ./
CMD ["markdown-meta-extension", "--yes"]

---
meta-data:
  title: Juxtapose Comparisons
  template: ./template.html
juxtapose: ./juxtapose-macro.html:juxtapose
---

# Juxtapose Comparisons

<https://juxtapose.knightlab.com>

{{ juxtapose
first:
  src: ./GoogleEarth-UniBremen-2002.jpg
  label: May 2002
  credit: Google Earth
second:
  src: ./GoogleEarth-UniBremen-2018.jpg
  label: April 2018
  credit: Google Earth
startingposition: "30%"
animate: True
}}

## Just web image links

{{ juxtapose
first: https://juxtapose.knightlab.com/static/img/Sochi_11April2005.jpg
second: https://juxtapose.knightlab.com/static/img/Sochi_22Nov2013.jpg
}}

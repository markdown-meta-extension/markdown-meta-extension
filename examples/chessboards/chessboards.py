from typing import Text

import chess
import chess.svg
from markupsafe import Markup

NAMESPACE_TO_REPLACE = (
    'xmlns="http://www.w3.org/2000/svg" '
    'xmlns:xlink="http://www.w3.org/1999/xlink"'
)

class ChessBoardWrapper(chess.Board):

    def move(self, move: Text) -> Markup:
        move = chess.Move.from_uci(move)
        self.push(move)
        return Markup(
            chess.svg.board(
                board=self,
                lastmove=move
            ).replace(NAMESPACE_TO_REPLACE, "")
        )

    def __html__(self) -> Markup:
        return Markup(
            str(
                chess.svg.board(
                    board=self
                )
            ).replace(NAMESPACE_TO_REPLACE, "")
        )

---
ChessBoard: ./chessboards.py:ChessBoardWrapper
---

# Chess boards using ``python-chess``

{{! board = ChessBoard }}

## Move 1

{{ board.move
move: a2a4
}}

{{ board.move
move: b8c6
}}

## Move 2

{{ board.move
move: b1c3
}}

{{ board.move
move: c6d4
}}

## Move 3

{{ board.move
move: e2e3
}}

{{ board.move
move: d4c2
}}

## Move 4

{{ board.move
move: d1c2
}}

{{ board.move
move: b7b5
}}

## Move 5

{{ board.move
move: b2b3
}}

{{ board.move
move: b5b4
}}

## Move 6

{{ board.move
move: c1a3
}}

{{ board.move
move: b4a3
}}

## Move 7

{{ board.move
move: e1a1
}}

> Long Castle Move

{{ board.move
move: a3a2
}}

## Move 8

{{ board.move
move: c1b2
}}

{{ board.move
move: a2a1q
}}

### Is white's king in check?

{{ board.is_check }}

### What's the number of moves after the current move?

{{ board.fullmove_number }}

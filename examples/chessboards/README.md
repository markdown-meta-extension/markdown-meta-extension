# Example: Creation and Manipulation of Chess Boards

## Used Libaries

* ``python-chess``
    - <https://python-chess.readthedocs.io/en/latest/index.html>
    - ``chess`` module

## Description

In this example the _Python_ library ``python-chess`` is used in the creation of a class ``ChessBoardWrapper``, which inherits from its class ``chess.Board``.

The only modifications necessary were the addition of the methods ``__html__()`` and ``move()``.

1. Method ``__html__()``
    - Returns the _SVG_ representation of the chess board object using the ``chess.svg`` module
2. Method ``move(Text)``
    - Accepts a _UCI_ ([Universal Chess Interface](https://en.wikipedia.org/wiki/Universal_Chess_Interface)) move (i.e. ``"a2a4"``)
    - Propagates it to the board
    - And returns the _SVG_ representation of the updated board with the move positions highlighted

> To prevent the _XML_ code of the _SVG_ representations from breaking in the ``Python-Markdown`` parser, the ``xmlns`` namespace is removed. Until a solution is found, this remains a necessary workaround for namespaced _XML_.

## Usage

### Front Matter

```yaml
---
ChessBoard: ./chessboards.py:ChessBoardWrapper
---
```

### New Chess Board with default Setup

```md
{{! board = ChessBoard }}
```

![Starting Board](./screenshots/StartingBoard.svg)

### New Chess Board with specific Setup

```md
{{! board = ChessBoard
fen: "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
}}
```

### Move from one Position to another

```md
{{ board.move
move: a2a4
}}
```

![Board after Move](./screenshots/MoveBoard.svg)

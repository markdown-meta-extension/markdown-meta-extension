from pathlib import Path
from typing import List, Text, Union

from markupsafe import Markup
import pandas
import xlrd
import yaml


class DataFrameWrapper(pandas.DataFrame):

    def __html__(self) -> Markup:
        return Markup(self.to_html())

    def __call__(self, **kwargs) -> Markup:
        return Markup(self.to_html(**kwargs))

    def __getitem__(self, *keys) -> Markup:
        result = self.loc[keys]
        return self.__class__(result)

    def __bool__(self) -> bool:
        return not self.empty


def read_excel(file_path: Text, **kwargs) -> DataFrameWrapper:
    data_frame = pandas.read_excel(file_path, **kwargs)

    return DataFrameWrapper(data_frame)


def read_ods(file_path: Text, **kwargs) -> DataFrameWrapper:
    if "engine" not in kwargs:
        kwargs["engine"] = "odf"

    return read_excel(file_path, **kwargs)


def read_csv(file_path: Text, **kwargs) -> DataFrameWrapper:
    data_frame = pandas.read_csv(file_path, **kwargs)

    return DataFrameWrapper(data_frame)


def read_json(file_path: Text, **kwargs) -> DataFrameWrapper:
    data_frame = pandas.read_json(file_path, **kwargs)

    return DataFrameWrapper(data_frame)


def read_yaml(file_path: Text, **kwargs) -> DataFrameWrapper:
    data_dict = yaml.safe_load(Path(file_path).read_text())

    data_frame = pandas.DataFrame.from_dict(data_dict, **kwargs)

    return DataFrameWrapper(data_frame)


EXTENSION_MAPPING = {
    ".xls": read_excel,
    ".xlsx": read_excel,
    ".ods": read_ods,
    ".csv": read_csv,
    ".json": read_json,
    ".yaml": read_yaml
}


def read_any(file_path: Text, **kwargs) -> DataFrameWrapper:
    file_path = Path(file_path)

    try:
        return EXTENSION_MAPPING[file_path.suffix](file_path, **kwargs)
    except KeyError as key_error:
        return key_error

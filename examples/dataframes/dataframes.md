---
DataTable: ./dataframes.py:read_any
ExcelTable: ./dataframes.py:read_excel
OdsTable: ./dataframes.py:read_ods
CsvTable: ./dataframes.py:read_csv
JsonTable: ./dataframes.py:read_json
YamlTable: ./dataframes.py:read_yaml
---

# Pandas DataFrame Examples

## Read any Table file

### Read ``demo.xls``

{{ DataTable
file_path: ./demo.xls
usecols:
- First Column
- Second Column
}}

#### Without Index

{{ without_index = DataTable
file_path: ./demo.xls
names:
- First
- Second
}}

{{ without_index
index: False
border: 0
}}

### Read ``demo.xlsx``

{{ DataTable
file_path: ./demo.xlsx
}}

### Read ``demo.ods``

{{ DataTable
file_path: ./demo.ods
}}

### Read ``demo.csv``

{{ DataTable
file_path: ./demo.csv
}}

### Read ``demo.json``

{{ DataTable
file_path: ./demo.json
}}

### Read ``demo.yaml``

{{ DataTable
file_path: ./demo.yaml
}}

## Read Excel files

### Read ``demo.xls``

{{ ExcelTable
file_path: ./demo.xls
}}

#### Read only second column of ``demo.xls``

{{ ExcelTable
file_path: ./demo.xls
usecols:
- Second Column
}}

### Read ``demo.xlsx``

{{ ExcelTable
file_path: ./demo.xlsx
}}

### Read ``demo.ods``

{{ ExcelTable
file_path: ./demo.ods
engine: odf
}}

## Read ODF files

### Read ``demo.ods``

{{ OdsTable
file_path: ./demo.ods
}}

## Read CSV files

### Read ``demo.csv``

{{ CsvTable
file_path: ./demo.csv
}}

## Read JSON files

### Read ``demo.json``

{{ JsonTable
file_path: ./demo.json
}}

## Read YAML files

### Read ``demo.yaml``

{{ YamlTable
file_path: ./demo.yaml
}}

## Larger Example: Postal Codes

{{ postal_codes_bremen = DataTable
file_path: ./BundeslandBremen.csv
sep: ";"
index_col:
- Stadtteil
}}

{{ postal_codes_bremen
index_names: False
}}

### Alphabetically sorted

{{ postal_codes_bremen.sort_index
inplace: True
}}

{{ postal_codes_bremen
index_names: False
}}

### A subset of the rows

{{ postal_codes_bremen.Horn-Lehe
index_names: False
}}

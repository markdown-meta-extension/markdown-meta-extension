from typing import Tuple

import folium
from markupsafe import Markup

class MapWrapper(folium.Map):

    def __html__(self) -> Markup:
        return Markup(self._repr_html_())

    def marker(
            self,
            location: Tuple[float, float],
            **kwargs
    ):
        marker = folium.Marker(location, **kwargs)
        marker.add_to(self)

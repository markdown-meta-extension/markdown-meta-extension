---
Map: ./maps.py:MapWrapper
---

# Interactive Maps using ``folium`` / ``leaflet.js``

## Uni Bremen Map

{{ uni_bremen_map = Map
location: [53.108659, 8.853607]
zoom_start: 16
}}

{{ uni_bremen_map.marker
location: [53.1066536, 8.8519653]
popup: Mehrzweckhochhaus (<b>MZH</b>)
tooltip: MZH
}}

{{ uni_bremen_map.marker
location: [53.108659, 8.853607]
popup: Staats- und Universitaetsbibliothek Bremen
}}

{{ uni_bremen_map.marker
location: [53.11, 8.858056]
popup: Fallturm Bremen
}}

{{ uni_bremen_map.marker
location: [53.106667, 8.845]
popup: Universum Bremen
}}

{{ uni_bremen_map }}

## Terrain Map

This is a map.

{{ map = Map
location: [45.372, -121.6972]
zoom_start: 12
tiles: Stamen Terrain
}}

{{ map.marker
location: [45.3288, -121.6625]
popup: "<i>Mt. Hood Meadows</i>"
tooltip: What <i>italic</i> lies here?
}}

{{ map.marker
location: [45.3311, -121.7113]
popup: "<b>Timberline Lodge</b>"
tooltip: What <b>bold</b> lies here?
}}

{{ map }}

And some more text.

## Toner Map Tiles

{{ Map
location:
- 45.523
- -122.675
tiles: Stamen Toner
}}

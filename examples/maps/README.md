# Example: Embedding and Manipulation of interactive Maps

## Used Libraries

* ``folium``
    - <https://python-visualization.github.io/folium/>
    - ``folium`` module
    - based on ``leaflet.js`` (<https://leafletjs.com/>)

## Description

In this example the _Python_ library ``folium``, which uses ``leaflet.js`` for visualization, is used for creating and working with interactive maps.

The custom class ``MapWrapper`` inherits from ``folium.Map``.

1. Method ``__html__()``
    - Returns the _HTML_ representation of the map by wrapping the method originally responsible for that.
    - The returned markup includes everything needed to display the map contained within an ``iframe``.
2. Method ``marker()``
    - Makes the addition of map markers centered on the map object.

## Usage

### Front Matter

```yaml
---
Map: ./maps.py:MapWrapper
---
```

### Adding a map

```md
{{ uni_bremen_map = Map
location: [53.108659, 8.853607]  # [lat, lon]
zoom_start: 16
}}
```

### Adding markers

```md
{{ uni_bremen_map.marker
location: [53.1066536, 8.8519653]  # [lat, lon]
popup: Mehrzweckhochhaus (<b>MZH</b>)
tooltip: MZH
}}

...

{{ uni_bremen_map }}
```

![Map of Uni Bremen with Markers](./screenshots/UniBremenMapWithMarkers.png)

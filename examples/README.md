# Example Use Cases for _Markdown Meta Extension_

## [Creation and Manipulation of Chess Boards](./chessboards/)

![Move on a Chess Board](./chessboards/screenshots/MoveBoard.svg)

## [Currency Conversion for Amounts of Money](./currencies/)

![Currency Conversion](./currencies/screenshots/CurrencyConversion.png)

## [Creation and Manipulation of Data Frames from Files](./dataframes/)

## [Creation of an interactive Juxtapose Slider Comparison](./juxtapose/)

![Juxtapose Slider Comparison](./juxtapose/screenshots/Juxtapose-GoogleEarth-UniBremen-Comparison.png)

## [Embedding and Manipulation of interactive Maps](./maps)

![Interactive Map with Markers](./maps/screenshots/UniBremenMapWithMarkers.png)

## [Creation of QR Codes from Data](./qrcodes)

## [Creation of interactive Timeline slides](./timeline)

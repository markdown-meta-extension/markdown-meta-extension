---
CurrencyAmount: ./currencies.py:CurrencyAmount
---

{{! semester_fee = CurrencyAmount
value: 382.67
currency: EUR
currencies:
- USD
- INR
- JPY
}}

The semester fee of **{{ semester_fee.EUR }}** would be worth {{ semester_fee.JPY }} in Japan and {{ semester_fee.GBP }} in Great Britain.

---

{{! budget = CurrencyAmount
value: 1250
currency: EUR
currencies:
- CAD
- USD
- INR
}}

Our budget contains **{{ budget.USD }}**, which is **{{ budget.INR }}**.

{{! ten_eur = CurrencyAmount
value: 10
currency: EUR
currencies:
- USD
- TRY
- JPY
- CNY
}}

In Great Britain it costs {{ ten_eur.GBP }}. In India the price is {{ ten_eur.INR }}.

{{! one_euro = CurrencyAmount
value: 1
currency: EUR
currencies:
- USD
- GBP
}}

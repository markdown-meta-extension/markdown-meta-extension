import datetime
from pathlib import Path
from typing import List, Text, Tuple, Union

from currency_converter import CurrencyConverter
import jinja2
from markupsafe import Markup


class CurrencyAmount:

    currency_converter = CurrencyConverter()
    template = jinja2.Template(Path("./currency_template.html").read_text())

    def __init__(
            self,
            value: float,
            currency: Text,
            currencies: Union[Text, List[Text]] = None,
            date: Union[Text, List, datetime.date] = None
    ):
        self.value = value

        self.currency = currency

        if not isinstance(currencies, list):
            currencies = [currencies]

        self.currencies = currencies

        if date:
            if isinstance(date, Text):
                date = datetime.date.fromisoformat(date)
            elif isinstance(date, List):
                date = datetime.date(*date)

            if isinstance(date, datetime.date):
                self.date = date
            else:
                self.date = None
        else:
            self.date = None

    def get_conversions(self) -> List[Tuple[float, Text]]:
        if not self.currencies:
            return list()

        return [
            (
                self.currency_converter.convert(
                    self.value,
                    self.currency,
                    currency,
                    self.date
                ),
                currency
            )
            for currency in self.currencies
        ]

    def __getitem__(self, key):
        value = self.currency_converter.convert(
            self.value,
            self.currency,
            key,
            self.date
        )
        return Markup(f"<span>{value:.2f} <em>{key}</em></span>")


    def __html__(self):
        return self.template.render(
            currency_value=self.value,
            currency_symbol=self.currency,
            currencies=self.get_conversions()
        )

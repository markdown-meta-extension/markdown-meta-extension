# Example: Currency Conversion for Amounts of Money

## Used Libraries

* ``CurrencyConverter``
    - <http://alexprengere.github.io/currencyconverter/>
    - ``currency_converter``

## Description

In this example the _Python_ library ``CurrencyConverter`` is used in the creation of the class ``CurrencyAmount``.

To not only make a currency amount representable, but also usable inline for arbitrary currency conversions, its class ``CurrencyAmount`` implements the following methods:

1. ``__init__(float, Text, [...])``
    - Initializes new instances with the decimal ``value`` in the three-letter ``currency``
    - Optionally a list of ``currencies`` can be provided, which is used for the conversions in the block representation
    - Optionally a historical ``date`` for the conversion can be provided (Seems not to work currently)
2. ``get_conversions()``
    - Returns a list of tuples with conversions defined in attribute ``currencies``, each in the format of ``(12.34, "EUR")``
3. ``__getitem__(Text)``
    - Provides access to the conversion for the given currency ``key``
    - Returns an inline _HTML_ representation of the converted currency amount
4. ``__html__()``
    - Returns the block representation of the currency amount with its representations by a rendering a _jinja_ template
    - The representation shows the currency conversions when expanded

## Usage

### Front Matter

```yaml
---
CurrencyAmount: ./currencies.py:CurrencyAmount
---
```

### Creation of a new Currency Amount

```md
{{! budget = CurrencyAmount
value: 1250
currency: EUR  # Euro
currencies:
- CAD          # Canadian Dollar
- USD          # US Dollar
- INR          # Indian Rupee
}}
```

![Currency Amount Block expanded](./screenshots/CurrencyAmountBlock.png)

### Inline Querying of Conversions

```md
Our budget contains **{{ budget.USD }}**, which is **{{ budget.INR }}**.
```

![Inline Conversion of a Currency Amount](./screenshots/CurrencyAmountInlineConversion.png)

import collections
import json
from pathlib import Path
import typing

from markupsafe import Markup
import yaml

class Dict(collections.UserDict):

    def __init__(
            self,
            data: typing.Union[typing.Text, typing.Dict] = None,
            **kwargs
    ):
        if isinstance(data, dict):
            pass
        elif isinstance(data, str):
            data_path = Path(data)

            if data_path.suffix.lower() == ".yaml":
                data = yaml.safe_load(data_path.read_text())
            elif data_path.suffix.lower() == ".json":
                data = json.load(data_path.read_text())
            else:
                data = dict()
        else:
            data = dict()

        super().__init__(data, **kwargs)

    def __html__(self) -> Markup:
        return Markup(
            json.dumps(dict(self), indent=2, ensure_ascii=False)
        )

---
meta-data:
  template: ./template.html
  title: Timelines
Dict: ./dicts.py:Dict
timeline: ./timeline_macros.html:timeline
---

# Timelines

## Timeline Universität Bremen

<https://www.uni-bremen.de/fileadmin/user_upload/sites/universitaetsarchiv/PDF/Publikationen/Zeitleiste_Uni_Bremen_fuer_Web_2018.pdf>

{{ timeline
id: uni-bremen-history
data: |
  {{ Dict
  data: ./uni_bremen_history_data.yaml
  }}
}}

import contextlib
import io
from typing import Text

from lxml import etree
from markupsafe import Markup
import qrcode
from qrcode.image.svg import SvgImage

class QRCodeWrapper(qrcode.QRCode):

    def __init__(self, data: Text, **kwargs):
        super().__init__(**kwargs)
        self.add_data(data)

    def __html__(self) -> Markup:
        image = self.make_image(SvgImage).get_image()
        return Markup(
            etree.tostring(image).decode("utf-8").replace(
                 'xmlns="http://www.w3.org/2000/svg"',
                ""
            )
        )

    def link(self, text: Text = None) -> Markup:
        link = self.data_list[0].data.decode("utf-8")

        if not text:
            text = link

        return Markup(f'<a href="{link}">{text}</a>')

    def ascii(self) -> Markup:
        pre_tag = etree.Element("pre")
        text_io = io.StringIO()
        with contextlib.redirect_stdout(text_io):
            self.print_ascii()

        text = text_io.getvalue().encode("ascii", "namereplace").decode()
        text = text.replace("{NO-BREAK SPACE}", "&nbsp;")
        text = text.replace("{FULL BLOCK}", "&#9608;")
        text = text.replace("{UPPER HALF BLOCK}", "&#9600;")
        text = text.replace("{LOWER HALF BLOCK}", "&#9604;")
        text = text.replace("\\N", "")
        pre_tag.text = text
        return Markup(etree.tostring(pre_tag).decode("utf-8"))

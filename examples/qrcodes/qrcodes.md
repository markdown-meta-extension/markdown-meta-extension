---
QR: ./qrcodes.py:QRCodeWrapper
---

# QR Codes

{{
    uni_bremen: https://www.uni-bremen.de/
}}

Please visit the website {{ uni_bremen_qr.link }} or {{ uni_bremen_qr.link [Uni Bremen] }} or use the QR Code below.

{{! uni_bremen_qr = QR
data: "{{ uni_bremen }}"
}}
